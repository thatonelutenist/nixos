# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      # User configuration
      ./common/user.nix
      # Application packages
      ./cachix.nix ./common/gpg.nix ./common/docker.nix
      ./common/applications/devel-core.nix ./common/applications/devel-rust.nix
      ./common/applications/utils-core.nix
      # Matrix homeserver configuration
      ./system-specific/matrix.community.rs/matrix.nix
      # Backup configuration
      ./sensitive/borg/matrix.community.rs.nix
      # ZT
      ./sensitive/zerotier.nix
      # gitea
      ./system-specific/matrix.community.rs/gitea.nix
    ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  # Grub configuration for linode
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only
  boot.loader.grub.forceInstall = true;
  boot.loader.timeout = 10;
  boot.loader.grub.extraConfig = ''
    serial --speed=19200 --unit=0 --word=8 --parity=no --stop=1;
    terminal_input serial;
    terminal_output serial
  '';
  boot.kernelParams = [
    "console=ttyS0"
  ];

  networking.hostName = "matrix"; 
  networking.domain = "community.rs";
  
  # Set your time zone.
  time.timeZone = "America/New_York";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp0s5.useDHCP = true;
  networking.enableIPv6 = false;

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  # };

  # Enable the X11 windowing system.
  # services.xserver.enable = true;

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.openssh.extraConfig = ''
    StreamLocalBindUnlink yes
  '';

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 22 ];
  networking.firewall.allowedUDPPorts = [ 22 ];
  networking.firewall.enable = true;

  # Turn on auto-upgrade
  system.autoUpgrade.enable = true;
  system.autoUpgrade.allowReboot = false;
  system.autoUpgrade.channel = https://nixos.org/channels/nixos-21.05;
  system.autoUpgrade.dates = "02:00";
  # Automatically maintain the store
  nix.autoOptimiseStore = true;
  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 30d";
  };

  # Only manage users through nixos
  users.mutableUsers = false;
  # Create www-html group
  users.groups.www-html.gid = 6848;
  # Add shaurya
  users.users.shaurya = {
    isNormalUser = true;
    home = "/home/shaurya";
    description = "Shaurya";
    extraGroups = [ "www-html" ];
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDA8BwFgWGrX5is2rQV+T0dy4MUWhfpE5EzYxjgLuH1V shauryashubham1234567890@gmail.com"
    ];
    shell = pkgs.nushell;
  };
  # Add www-html for my self
  users.users.nathan = {
    extraGroups = [ "www-html" ];
  };
  # Make sure ssh password authentication is off
  services.openssh.passwordAuthentication = false;

  
  system.stateVersion = "21.05";
}

