{ config, pkgs, ... }:
{
  services.plex = let
    myPlexRaw = pkgs.plexRaw.overrideAttrs (x: let
      # see https://www.plex.tv/media-server-downloads/ for 64bit rpm
      version = "1.24.4.5081-e362dc1ee";
      sha256 = "17igy0lq7mjmrw5xq92b57np422x8hj6m8qzjri493yc6fw1cl1m";
    in {
      name = "plex-${version}";
      src = pkgs.fetchurl {
        url = "https://downloads.plex.tv/plex-media-server-new/${version}/debian/plexmediaserver_${version}_amd64.deb";
        inherit sha256;
      };
    }
    );
    myPlex = pkgs.plex.override (x: {plexRaw = myPlexRaw; });
  in
    {
      enable = true;
      openFirewall = true;
      dataDir = "/var/lib/plex";
      user = "nathan";
      group = "users";
      package = myPlex;
    };

  services.tautulli.enable = true;
}
