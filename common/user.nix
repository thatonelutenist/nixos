# Contains general user environment configuration
{ config, pkgs, ...}:
let
  unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };      
in
{
  # Import password
  imports = [ ../sensitive/password.nix ];
  
  
  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;
  
  # Set time zone
  time.timeZone = "America/New_York";
  
  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };
  
  # enable sudo
  security.sudo.enable = true;
  
  # Enable fish as a login shell
  environment.shells = [ pkgs.bashInteractive unstable.fish ];
  users.users.nathan = {
    isNormalUser = true;
    home = "/home/nathan";
    description = "Nathan McCarty";
    extraGroups = [ "wheel" "networkmanager" "audio" "docker" "libvirtd" "uinput" "adbusers"];
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILRs6zVljIlQEZ8F+aEBqqbpeFJwCw3JdveZ8TQWfkev cardno:000615938515"
    ];
    shell = unstable.fish;
  };

  # Install general use packages
  environment.systemPackages = with pkgs; [
    # cachix for nix cache management
    cachix
    # Install our shell of choice
    unstable.fish
  ];
}
