{ config, pkgs, ... }:
{
  imports = [
    ./applications/emacs.nix
    ./applications/devel-core.nix
    ./applications/devel-rust.nix
    ./applications/utils-core.nix
    ./applications/communications.nix
    ./applications/media.nix
    ./applications/syncthing.nix
    ./applications/image-editing.nix
    ./applications/tablet.nix
  ];
}
