# X11 and other graphical program configuration
{ config, pkgs, ... }:
let
  unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };
  myCustomLayout = pkgs.writeText "xkb-layout" ''
    keycode 202 = Hyper_L
    remove mod4 = Hyper_L
    add mod3 = Hyper_L
  '';  
in
{
  nixpkgs.overlays =
  let
    # Change this to a rev sha to pin
    moz-rev = "master";
    moz-url = builtins.fetchTarball { url = "https://github.com/mozilla/nixpkgs-mozilla/archive/${moz-rev}.tar.gz";};
    nightlyOverlay = (import "${moz-url}/firefox-overlay.nix");
  in [
    nightlyOverlay
  ];
  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;
    autorun = true;
    displayManager.gdm = {
      enable = true;
      wayland = true;
    };
    desktopManager.plasma5.enable = true;
  };
  # enable 32 bit graphics drivers for older games
  hardware.opengl.driSupport32Bit = true;
  # Install some core graphical utilities
  environment.systemPackages = with pkgs; [
    # Firefox, so we can actually use the internet
    latest.firefox-nightly-bin
    # Alacritty, our terminal emulator
    unstable.alacritty
    # KDEConnect for interfacing with our phone
    kdeconnect
    # xmodmap for key remapping
    xorg.xmodmap
    # GTK theming
    gtk-engine-murrine gtk_engines gsettings-desktop-schemas lxappearance
  ];
  # QT theming
  programs.qt5ct.enable = true;

  # Remap f24 to hyper
  # services.xserver.displayManager.sessionCommands = "${pkgs.xorg.xmodmap}/bin/xmodmap ${myCustomLayout}";

  # Enable noise torch
  programs.noisetorch.enable = true;

  # Enable sway
  programs.sway = {
    enable = true;
    wrapperFeatures.gtk = true; # so that gtk works properly
    extraPackages = with pkgs; [
      # Waybar for the bar
      unstable.waybar
      # Locking and screen management
      wdisplays
      swaylock-effects
      unstable.swayidle
      # Clipboard
      wl-clipboard
      # Notifications
      mako
      # Terminal
      alacritty
      # Glib for sound stuff
      glib
      # Glpaper for glsl shaders as background
      glpaper
      # Grimshot for screen capture
      sway-contrib.grimshot
    ];
  };
  

  # Enable xdg-portal
  xdg = {
    portal = {
      enable = true;
      extraPortals = with pkgs; [
        xdg-desktop-portal-wlr
        xdg-desktop-portal-gtk
      ];
      gtkUsePortal = true;
    };
  };

  # Tell firefox to use wayland
  environment.sessionVariables = {
    MOZ_ENABLE_WAYLAND = "1";
    XDG_CURRENT_DESKTOP = "sway";
    # XDG_SESSION_TYPE = "wayland";
    QT_QPA_PLATFORMTHEME = "qt5ct";
  };
}
