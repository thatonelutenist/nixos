# Core development libraries
{ config, pkgs, ... }:
let
  unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };  
in
{
  environment.systemPackages = with pkgs; [
    # Full version of git
    unstable.gitFull
    # Git addons
    git-secret
    # General development requirements
    python3Full cmake gcc unstable.libvterm-neovim libtool binutils clang gnumake
  ];
}
