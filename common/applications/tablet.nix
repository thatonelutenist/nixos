# Support for using my android tablet as a graphics tablet
{ config, pkgs, ... }:
{
  # Pull in personal overlay
  nixpkgs.overlays = [(import ../../overlays/personal/overlay.nix)];
  # Configure uinput rules
  services.udev.extraRules = ''
    echo 'KERNEL=="uinput", MODE="0660", GROUP="uinput", OPTIONS+="static_node=uinput"'
 '';
  boot.kernelModules = [ "uinput" ];
  # install package
  environment.systemPackages = with pkgs;
    let
      weylus-desktop = pkgs.makeDesktopItem {
        name = "Weylus";
        desktopName = "Weylus";
        exec = weylus + "/bin/weylus";
        terminal = false;
        comment = "Use your tablet as graphic tablet/touch screen on your computer.";
        icon = "input-tablet";
      };
    in
    [weylus weylus-desktop xournalpp];
}
