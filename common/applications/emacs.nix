{ config, pkgs, ... }:
let
  myEmacs = ((pkgs.emacsPackagesFor pkgs.emacsPgtkGcc).emacsWithPackages [
    pkgs.emacsPackagesNg.emacs-libvterm
    pkgs.emacsPackagesNg.multi-vterm
  ]);
in
{
  # Import emacs overlay
  nixpkgs.overlays = [
    (import (builtins.fetchTarball {
      url = https://github.com/nix-community/emacs-overlay/archive/master.tar.gz;
    }))  
  ];
  # Install emacs
  environment.systemPackages = with pkgs; [
    myEmacs
  ];
  # Enable emacs service
  services.emacs = {
    enable = true;
    package = myEmacs;
  };
}
