# Communications software
{ config, pkgs, ...}:
let
  unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };
  masterTarball =
    fetchTarball
      https://github.com/NixOS/nixpkgs/archive/refs/heads/master.tar.gz;
in
{
  # Pull in personal overlay
  nixpkgs.overlays = [(import "/etc/nixos/overlays/personal/overlay.nix")];
  # Make packages from unstable nixos available
  nixpkgs.config = {
    packageOverrides = pkgs: {
      master = import masterTarball {
        config = config.nixpkgs.config;
      };
    };
  };
  environment.systemPackages = with pkgs;
    let
      ## Wayland workaround chromium desktop items
      # Facebook messenger 
      fbChromeDesktopItem = pkgs.makeDesktopItem {
        name = "messenger-chrome";
        desktopName = "Messenger (chrome)";
        exec = "${pkgs.chromium}/bin/chromium --enable-features=UseOzonePlatform -ozone-platform=wayland --app=\"https://messenger.com\"";
        terminal = false;
      };
      # Teams
      teamsItem = pkgs.makeDesktopItem {
        name = "teams-wayland";
        desktopName = "Teams (Wayland)";
        exec = "${pkgs.chromium}/bin/chromium --enable-features=UseOzonePlatform -ozone-platform=wayland --app=\"https://teams.microsoft.com\"";
        terminal = false;
      };
      ## Pass wayland options to existing applications
      signalWaylandItem = pkgs.makeDesktopItem {
        name = "signal-desktop-wayland";
        desktopName = "Signal (Wayland)";
        exec = "${pkgs.signal-desktop}/bin/signal-desktop --enable-features=UseOzonePlatform -ozone-platform=wayland";
        terminal = false;
        icon = "signal-desktop";
        type = "Application";
      };
    in
    [
    # Discord
    discord-wayland unstable.betterdiscordctl
    # Use unstable element for latest features
    master.element-desktop-wayland
    # Desktop signal client
    signal-desktop signalWaylandItem
    # Desktop telegram client
    tdesktop
    # chromium
    chromium
    # Wayland workaround packages
    fbChromeDesktopItem teamsItem
  ];
}
