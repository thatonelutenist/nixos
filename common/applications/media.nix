# Media players and other applications
{ config, pkgs, ... }:
let
  unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };
in
{
  imports = [ ../../sensitive/mopidy.nix ];
  environment.systemPackages = with pkgs; [
    # Spotify
    spotify
    # Latest version of vlc
    unstable.vlc
    # Plex client
    plex-media-player
    # OBS studio for screen recording
    unstable.obs-studio
  ];
}
