# Utilities for developing in rust
{ config, pkgs, ... }:
let
  unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };  
in
{
  environment.systemPackages = with pkgs; [
    # Use rustup to get the compiler
    rustup
    # Install the latest rust analyzer
    unstable.rust-analyzer
    # Sccache and lld for faster builds
    sccache lld_12
    # Misc cargo utilites
    cargo-binutils cargo-edit cargo-asm cargo-fuzz cargo-license cargo-criterion
  ];
}
