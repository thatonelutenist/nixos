{ config, pkgs, ... }:
let
  unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };
in
{
  # Enable docker and use unstable version
   virtualisation.docker = {
    enable = true;
    package = unstable.docker;
    # Automatically prune to keep things lean
    autoPrune.enable = true;
  };
}
