{ config, pkgs, ... }:
{
  # Install keybase
  environment.systemPackages = with pkgs; [
    keybase keybase-gui
  ];
  # Turn on keybase fs
  services.kbfs = {
    enable = true;
    mountPoint = "%t/kbfs";
    extraFlags = ["-label %u"];
  };
  # Setup slice and enable user services
  systemd.user.services = {
    keybase.serviceConfig.Slice = "keybase.slice";
    
    kbfs = {
      environment = { KEYBASE_RUN_MODE = "prod"; };
      serviceConfig.Slice = "keybase.slice";
    };
    
    keybase-gui = {
      description = "Keybase GUI";
      requires = [ "keybase.service" "kbfs.service" ];
      after    = [ "keybase.service" "kbfs.service" ];
      serviceConfig = {
        ExecStart  = "${pkgs.keybase-gui}/share/keybase/Keybase";
        PrivateTmp = true;
        Slice      = "keybase.slice";
      };
    };
  };

}
