{ config, pkgs, ... }:
{
  # Setup logitech wireless support
  hardware.logitech.wireless.enable = true;
  hardware.logitech.wireless.enableGraphical = true;  
}
