# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:
let
  unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };
in
{
  nixpkgs.config.tarball-ttl = 0;
  nixpkgs.overlays = let
    mbp-overlay = builtins.fetchTarball {
      url = https://github.com/t2linux/nixos-overlay/archive/refs/heads/master.tar.gz;
    };
  in [
    (import (mbp-overlay + "/overlay.nix"))
  ];
  imports =
    [ 
      ./hardware-configuration.nix ./overlays/t2linux/modules/wifi-fw-selection.nix ./cachix.nix
      ./common/user.nix ./common/logitech.nix ./common/audio.nix ./common/gpg.nix
      ./common/graphical.nix ./common/keybase.nix ./common/fonts.nix ./common/applications-all.nix
      ./common/docker.nix ./common/qemu.nix ./common/adb.nix ./sensitive/borg/mbp161.nix
      ./sensitive/zerotier.nix ./common/ssh.nix
    ];

  # Improves system stability - https://canary.discord.com/channels/595304521857630254/595304521857630259/809475825589026826
  # Disable built in audio, as it's not working that well - 2021-05-12
  boot.kernelParams = [ "intel_iommu=on" "apple_bce.aaudio_enabled=0" ];
  # Use custom kernel
  boot.kernelPackages = pkgs.linuxPackagesFor pkgs.linux-mbp;
  boot.extraModulePackages = with pkgs; [ apple-bce apple-ib-drv ];
  # Load Apple hardware modules early
  boot.initrd.kernelModules = [ "apple_bce" "apple-ibridge" "apple-ib-tb" ];
  boot.kernelModules = ["applesmc"];
  # Include wifi firmware
  hardware.appleWifiFirmware.model = "MacBookPro16,1";
  hardware.firmware = [ (pkgs.apple-wifi-firmware.override { macModel = config.hardware.appleWifiFirmware.model; }) ];
  # Allow unfree - wifi firmware won't be installed otherwise.
  nixpkgs.config.allowUnfree = true;

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.efiSupport = true;
  boot.loader.grub.efiInstallAsRemovable = true;
  boot.loader.efi.efiSysMountPoint = "/boot/";
  boot.loader.efi.canTouchEfiVariables = false;
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.device = "nodev";
  # Add boot entries to switch gpu
  boot.loader.grub.extraEntries = ''
    menuentry "Enable iGPU for future boots with gpu-power-prefs=1, and shutdown." --class tool {
        chainloader /gpu-power-prefs-igpu-x86_64.efi
      }
    menuentry "Revert to dGPU for future boots with gpu-power-prefs=0, and shutdown." --class tool {
        chainloader /gpu-power-prefs-dgpu-x86_64.efi
    }
  '';
  # Enable luks
  boot.initrd.luks.devices.luksroot = {
    device = "/dev/disk/by-uuid/0582d21c-652a-46f9-a041-9e59f7d65f34";
    preLVM = true;
    allowDiscards = true;
  };

  # suspend/resume is quite broken - 2021-05-12
  services.logind.lidSwitch = "ignore";
  services.logind.lidSwitchDocked = "ignore";
  services.logind.lidSwitchExternalPower = "ignore";

  networking = {
    hostName = "extremophile";
    domain = "mccarty.io";
    useDHCP = false;
    networkmanager = {
      enable = true;
      dns = "none";
    };
    nameservers = [ "172.23.98.121" ];
  };

  environment.systemPackages = with pkgs; [
    # Setup ability to mount apfs volumes
    apfs-fuse
  ];
  
  # List packages installed in system profile. To search, run:
  # $ nix search wget
  # Enable steam, this is a bit wonky
  programs.steam.enable = true;

  # Configure X11 for this machine
  services.xserver = {
    videoDrivers = ["amdgpu" "modesetting"];
    useGlamor = true;
    logFile = "/var/log/Xorg.0.log";
  };
  
  # Enable CUPS to print documents.
  services.printing.enable = true;
  
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  networking.firewall.enable = false;

  # Enable binfmt so we can run arm containers and wasm-wasi binaries
  boot.binfmt.emulatedSystems = ["wasm32-wasi" "aarch64-linux"];

  # Configure cpu scaling
  powerManagement = {
    enable = true;
    cpuFreqGovernor = "ondemand";
  };
  # Tune the scheduler to be a little more desktop oriented
  boot.kernel.sysctl = {
    "vm.swappiness" = 1; # when swapping to ssd, otherwise change to 1
    "vm.vfs_cache_pressure" = 50;
    "vm.dirty_background_ratio" = 20;
    "vm.dirty_ratio" = 50;
    # these are the zen-kernel tweaks to CFS defaults (mostly)
    "kernel.sched_latency_ns" = 4000000;
    # should be one-eighth of sched_latency (this ratio is not
    # configurable, apparently -- so while zen changes that to
    # one-tenth, we cannot):
    "kernel.sched_min_granularity_ns" = 500000;
    "kernel.sched_wakeup_granularity_ns" = 50000;
    "kernel.sched_migration_cost_ns" = 250000;
    "kernel.sched_cfs_bandwidth_slice_us" = 3000;
    "kernel.sched_nr_migrate" = 128;
  };

  # enable hardware accelerated video rendering
  hardware.opengl = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver 
      vaapiIntel         
      vaapiVdpau
      libvdpau-va-gl
    ];
    extraPackages32 = with pkgs.pkgsi686Linux; [
      vaapiIntel         
    ];
  };

  # enable the btrfs dedup agent
  services.beesd.filesystems = {
    root = {
      spec = "UUID=6a551eb6-b725-4d87-860c-530e78cbfabf";
      hashTableSizeMB = 512;
      verbosity = "crit";
      extraOptions = [ "--loadavg-target" "1.0" ];
    };
  };

  # Enable flatpak, we need this for caprine
  services.flatpak.enable = true;

  # Enable fan (kernel does not have applesmc so this does not work)
  # services.mbpfan.enable = true;
  
  system.stateVersion = "21.05"; 
}

