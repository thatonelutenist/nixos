{ config, pkgs, lib, ... }:

{
  imports =
    [
      "${fetchTarball "https://github.com/NixOS/nixos-hardware/archive/936e4649098d6a5e0762058cb7687be1b2d90550.tar.gz" }/raspberry-pi/4"
      ./cachix.nix ./common/user.nix ./common/applications/utils-core.nix ./common/applications/devel-core.nix
      ./sensitive/zerotier.nix ./sensitive/tounge/acme.nix ./system-specific/tounge/nginx.nix
    ];

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
      options = [ "noatime" ];
    };
  };

  networking = {
    hostName = "tounge";
    domain = "mccarty.io";
    wireless.enable = false;
  };
  networking.interfaces.eth0.ipv4.addresses = [ {
    address = "10.0.0.10";
    prefixLength = 21;
  } ];
  networking.defaultGateway = "10.0.4.1";
  networking.nameservers = [ "1.1.1.1" "1.0.0.1" ];

  time.timeZone = "America/New_York";

  services.openssh.enable = true;
  services.openssh.extraConfig = ''
    StreamLocalBindUnlink yes
    '';
  services.openssh.listenAddresses = [
    {
      addr = "0.0.0.0";
      port = 22;
    }
  ];

  users.mutableUsers = false;

  # Turn on auto-upgrade
  system.autoUpgrade.enable = true;
  system.autoUpgrade.allowReboot = false;
  system.autoUpgrade.channel = https://nixos.org/channels/nixos-21.05;
  system.autoUpgrade.dates = "02:00";
  # Automatically maintain the store
  nix.autoOptimiseStore = true;
  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 7d";
  };

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 22 53 3080 30443];
  networking.firewall.allowedUDPPorts = [ 22 53 ];
  networking.firewall.enable = true;
  # Trust zerotier interface
  networking.firewall.trustedInterfaces = [ "zt5u4uutwm" ];

  # Setup environment for gpg agent
  environment.shellInit = ''
  export GPG_TTY="$(tty)"
  gpg-connect-agent /bye
  export SSH_AUTH_SOCK="/run/user/$UID/gnupg/S.gpg-agent.ssh"
'';

  environment.sessionVariables = {
    SSH_AUTH_SOCK = "/run/user/1001/gnupg/S.gpg-agent.ssh";
  };

  programs = {
    # Disable ssh-agent, the gpg-agent will fill in
    ssh.startAgent = false;
    # Enable gpg-agent with ssh support
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
      enableExtraSocket = true;
    };
  };

  # Setup pihole
  docker-containers.pihole = {
    image = "pihole/pihole:latest";
    ports = [
      "10.0.0.10:53:53/tcp"
      "10.0.0.10:53:53/udp"
      "172.23.98.121:53:53/tcp"
      "172.23.98.121:53:53/udp"
      "3080:80"
      "30443:443"
    ];
    volumes = [
      "/var/lib/pihole/:/etc/pihole/"
      "/var/lib/dnsmasq.d:/etc/dnsmasq.d/"
    ];
    extraDockerOptions = [
      "--cap-add=NET_ADMIN"
      "--dns=1.1.1.1"
    ];
    workdir = "/var/lib/pihole/";
  };

  # Setup heimdall
  docker-containers.heimdall = {
    image = "linuxserver/heimdall";
    ports = [ "3081:80" ];
  };

  system.stateVersion = "21.05";   
}
