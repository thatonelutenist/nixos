# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./cachix.nix ./common/user.nix ./common/applications/utils-core.nix ./common/applications/devel-core.nix
      ./sensitive/zerotier.nix ./common/gpg.nix ./common/plex.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  time.timeZone = "America/New_York";

  networking = {
    hostName = "perception";
    domain = "mccarty.io";
    useDHCP = false;
    interfaces.eno1 = {
      useDHCP = false;
      ipv4.addresses = [
        {
          address = "10.0.0.11";
          prefixLength = 21;
        }
      ];
    };
    defaultGateway = "10.0.4.1";
    nameservers = [ "10.0.0.10" ];
  };

  i18n.defaultLocale = "en_US.UTF-8";

  services.openssh.enable = true;
  services.openssh.extraConfig = ''
    StreamLocalBindUnlink yes
    '';

  networking.firewall.enable = true;
  # Trust ZT interface
  networking.firewall.trustedInterfaces = [ "zt5u4uutwm" ];

  users.mutableUsers = false;
  
  # Turn on auto-upgrade
  system.autoUpgrade.enable = true;
  system.autoUpgrade.allowReboot = false;
  system.autoUpgrade.channel = https://nixos.org/channels/nixos-21.05;
  system.autoUpgrade.dates = "02:00";
  # Automatically maintain the store
  nix.autoOptimiseStore = true;
  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 7d";
  };

  # add plex nfs mount
  fileSystems."/var/plex" = {
    device = "10.0.3.10:/zfs/data/plex";
    fsType = "nfs";
  };

  system.stateVersion = "21.05"; # Did you read the comment?

}

