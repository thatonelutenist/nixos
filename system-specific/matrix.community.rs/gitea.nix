{ config, pkgs, lib, ... }:
{
  services.gitea = {
    enable = true;
    appName = "Rust Community Matrix Homeserver";
    database = {
      type = "sqlite3";
    };
    domain = "gitea.community.rs";
    rootUrl = "https://gitea.community.rs";
    httpPort = 3001;
    settings = {
      service = {
        DISABLE_REGISTRATION = lib.mkForce true;
      };
    };
  };

  services.nginx = {
    virtualHosts."gitea.community.rs" = {
      enableACME = true;
      forceSSL = true;
      locations."/".proxyPass = "http://localhost:3001";
    };
  };
}
