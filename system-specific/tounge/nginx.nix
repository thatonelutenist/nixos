{ pkgs, config, lib, ... }:
{
  networking.firewall.allowedTCPPorts = [ 80 443];
  networking.firewall.allowedUDPPorts = [ 80 443 ];
  services.nginx = {
    enable = true;
    recommendedTlsSettings = true;
    recommendedOptimisation = true;
    recommendedGzipSettings = true;
    recommendedProxySettings = true;
  
    virtualHosts = {
      "pihole.mccarty.io" = {
        forceSSL = true;
        useACMEHost = "mccarty.io";
        locations."/" = {
          proxyPass = "http://localhost:3080";
          extraConfig = ''
            allow 172.23.0.0/16;
            deny all;
          '';
        };
      };
      "hub.mccarty.io" = {
        forceSSL = true;
        useACMEHost = "mccarty.io";
        locations."/" = {
          proxyPass = "http://localhost:3081";
          extraConfig = ''
            allow 172.23.0.0/16;
            deny all;
          '';
        };
      };
      "sonarr.mccarty.io" = {
        forceSSL = true;
        useACMEHost = "mccarty.io";
        locations."/" = {
          proxyPass = "http://10.0.3.10:8989";
          extraConfig = ''
            allow 172.23.0.0/16;
            deny all;
          '';
        };
      };
      "radarr.mccarty.io" = {
        forceSSL = true;
        useACMEHost = "mccarty.io";
        locations."/" = {
          proxyPass = "http://10.0.3.10:7878";
          extraConfig = ''
            allow 172.23.0.0/16;
            deny all;
          '';
        };
      };
      "sabnzbd.mccarty.io" = {
        forceSSL = true;
        useACMEHost = "mccarty.io";
        locations."/" = {
          proxyPass = "http://10.0.3.10:8080";
          extraConfig = ''
            allow 172.23.0.0/16;
            deny all;
          '';
        };
      };
      
    };
  };
}
