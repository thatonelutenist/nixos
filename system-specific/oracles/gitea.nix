{ config, pkgs, lib, ... }:
{
  services.gitea = {
    enable = true;
    appName = "Nathan's Git";
    database = {
      type = "sqlite3";
    };
    domain = "git.mccarty.io";
    rootUrl = "https://git.mccarty.io";
    httpPort = 3001;
    settings = {
      ui = {
        DEFAULT_THEME = "arc-green";
      };
      service = {
        DISABLE_REGISTRATION = lib.mkForce true;
      };
    };
    lfs.enable = true;
  };

  services.nginx = {
    virtualHosts."git.mccarty.io" = {
      enableACME = true;
      forceSSL = true;
      locations."/".proxyPass = "http://localhost:3001";
    };
  };
}
