{ pkgs, lib, config, ... }:
let
  masterTarball =
    fetchTarball
      https://github.com/NixOS/nixpkgs/archive/refs/heads/master.tar.gz;
in
{
  # Make packages from unstable nixos available
  nixpkgs.config = {
    packageOverrides = pkgs: {
      master = import masterTarball {
        config = config.nixpkgs.config;
      };
    };
  };
  services.postgresql.enable = true;
  services.postgresql.initialScript = pkgs.writeText "synapse-init.sql" ''
    CREATE ROLE "matrix-synapse" WITH LOGIN PASSWORD 'matrix-synapse';
    CREATE DATABASE "synapse" WITH OWNER "synapse"
      TEMPLATE template0
      LC_COLLATE = "C"
      LC_CTYPE = "C";
  '';

  services.nginx = {
    virtualHosts = {
      "matrix.mccarty.io" = {
        enableACME = true;
        forceSSL = true;

        locations."/".extraConfig = ''
          rewrite ^(.*)$ http://element.mccarty.io$1 redirect;
        '';

        # forward all Matrix API calls to the synapse Matrix homeserver
        locations."/_matrix" = {
          proxyPass = "http://[::1]:8008"; # without a trailing /
        };
      };
      "element.mccarty.io" = {
        enableACME = true;
        forceSSL = true;
        root = pkgs.master.element-web;
      };
    };
  };

  services.matrix-synapse = {
    enable = true;
    server_name = "mccarty.io";
    listeners = [
      {
        port = 8008;
        bind_address = "::1";
        type = "http";
        tls = false;
        x_forwarded = true;
        resources = [
          {
            names = [ "client" "federation" ];
            compress = false;
          }
        ];
      }
    ];
    database_user = "matrix-synapse";
    database_name = "synapse";
    extraConfig = ''
      ip_range_whitelist:
        - '172.23.0.0/16'
    '' ;           
  };
}
